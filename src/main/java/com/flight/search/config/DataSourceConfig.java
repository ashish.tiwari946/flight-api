package com.flight.search.config;


import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mysql.cj.jdbc.MysqlDataSource;

@Configuration
public class DataSourceConfig {

	@Value("${spring.datasource.username}")
	private String username;

	@Value("${spring.datasource.password}")
	private String password;

	@Value("${spring.datasource.url}")
	private String url;

	@Bean
	public DataSource datasource() throws SQLException {
		MysqlDataSource datasource = new MysqlDataSource();
		datasource.setUser(username);
		datasource.setPassword(password);
		datasource.setURL(url);
		// datasource.setImplicitCachingEnabled(true);
		// datasource.setFastConnectionFailoverEnabled(true);
		return datasource;
	}
}
