package com.flight.search.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.flight.search.model.FlightEntity;

@Repository
public interface FlightRepository extends JpaRepository<FlightEntity, Long> {

	@Query("Select fs from FlightEntity fs where fs.origin = :origin and fs.destination = :destination and fs.departure BETWEEN :startDate AND :endDate")
	List<FlightEntity> searchByOrigin(@Param("origin") String origin, @Param("destination") String destination,
			@Param("startDate") Date date1, @Param("endDate") Date date2);

	@Query("Select fs from FlightEntity fs where fs.flightNumber = :flightNumber and fs.departure BETWEEN :startDate AND :endDate")
	List<FlightEntity> getFlightDetailsByNumber(@Param("flightNumber") String flightNumber,
			@Param("startDate") Date date1, @Param("endDate") Date date2);

}
