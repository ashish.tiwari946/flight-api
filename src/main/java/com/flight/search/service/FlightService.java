package com.flight.search.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.assertj.core.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flight.search.model.FlightEntity;
import com.flight.search.repository.FlightRepository;

@Service
public class FlightService {

	@Autowired
	private FlightRepository flightRepository;

	public List<FlightEntity> getFlightDetails(String origin, String destination, String departs)
			throws ParseException {
		String newDate = modifyDateLayout(departs);
		Date date1 = getDatePlus(newDate);
		Date date2 = getDateMinus(newDate);
		List<FlightEntity> flightsList = flightRepository.searchByOrigin(origin, destination, date2, date1);
		return flightsList;
	}

	public List<FlightEntity> getFlightDetailsByNumber(String flightNumber, String departs) throws ParseException {
		String newDate = modifyDateLayout(departs);
		Date date1 = getDatePlus(newDate);
		Date date2 = getDateMinus(newDate);
		List<FlightEntity> flightsList = flightRepository.getFlightDetailsByNumber(flightNumber, date2, date1);
		return flightsList;
	}

	private Date getDatePlus(String newDate) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		String datePlus;
		Calendar c = Calendar.getInstance();
		c.setTime(format.parse(newDate));
		c.add(Calendar.DATE, 1); // number of days to add
		datePlus = format.format(c.getTime());
		Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS").parse(datePlus);
		return date1;
	}

	private Date getDateMinus(String newDate) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		Calendar cal = Calendar.getInstance();
		cal.setTime(format.parse(newDate));
		cal.add(Calendar.DATE, -1);
		String dateMinus = format.format(cal.getTime());
		Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS").parse(dateMinus);
		return date1;
	}

	private String modifyDateLayout(String inputDate) throws ParseException {
		inputDate = inputDate.replace(".000Z", "");
		Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(inputDate);
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}
}
