package com.flight.search.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "flight_details")
public class FlightEntity {

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "flight_number")
	private String flightNumber;

	@Column(name = "carrier")
	private String carrier;

	@Column(name = "origin")
	private String origin;

	@Column(name = "departure")
	private Date departure;

	@Column(name = "destination")
	private String destination;

	@Column(name = "arrival")
	private Date arrival;

	@Column(name = "aircraft")
	private String aircraft;

	@Column(name = "distance")
	private int distance;

	@Column(name = "travel_time")
	private String travelTime;

	@Column(name = "status")
	private String status;
}
