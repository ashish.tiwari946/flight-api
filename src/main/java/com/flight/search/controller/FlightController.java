package com.flight.search.controller;

import java.sql.Date;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flight.search.model.FlightEntity;
import com.flight.search.repository.FlightRepository;
import com.flight.search.service.FlightService;

@RestController
@RequestMapping("/api")
public class FlightController {

	@Autowired
	private FlightService flightService;
	
	@Autowired
	private FlightRepository flightRepo;

	@GetMapping("/flight/{origin}/{destination}/{departs}")
	public ResponseEntity<?> getFlightDetails(@PathVariable("origin") String origin,
			@PathVariable("destination") String destination, @PathVariable("departs") String departs) throws ParseException {
		List<FlightEntity> list = flightService.getFlightDetails(origin, destination, departs);
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
	
	@GetMapping("/flight/number/{flightNumber}/{departs}")
	public ResponseEntity<?> getFlightDetailsByNumber(@PathVariable("flightNumber") String flightNumber,
			@PathVariable("departs") String departs) throws ParseException {
		List<FlightEntity> list = flightService.getFlightDetailsByNumber(flightNumber, departs);
		return new ResponseEntity<>(list,HttpStatus.OK);
	}


	@GetMapping("/flights")
	public ResponseEntity<?> getFlightS() {
		List<FlightEntity> list = flightRepo.findAll();
		return new ResponseEntity<>(list,HttpStatus.OK);
	}
}
